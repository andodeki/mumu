use std::fs::File;
use std::io::{self, Write};

fn main() -> io::Result<()> {
    let contacts = vec![
        ("Dershi Vishram Versani", "0722410530", "neelbid@yahoo.com"),
        ("Solomon Njenga", "0727646358", "ranciengineerin@gmail.com"),
        ("Mutemi Ndonga", "0724686448", "mutemindonga@gmail.com"),
        ("ERICMUTUNE", "0710158374", "mestumerica@ghail.com"),
        ("SHREEKANT KERAI", "0731337014", "pscl@seyani.com"),
        ("Mangela Musyoka", "0722614616", "hidayacom@yahoo.com"),
        ("Rhoda rugendo", "0719382929", "huashieastafrica@gmail.com"),
        ("Kenneth Mwenda", "0723127076", "project31@burhaniengineers.com"),
        ("RAMESH VSIHRAM", "0722515390", "kenya@cementers.com"),
        ("WILLY KIPROTCH", "0705167161", "INFO@MILICONS.COM"),
        ("IVY NJOKI", "0726150870", "info@mwembeandmwembe.co.ke"),
        ("Erick Ouko", "0722290050", "Info@orionnebulaltd.co.ke"),
        ("GEORGE K MGURE", "0721438066", "ngare@seyanibrosk.com"),
        ("BRUCE N. GCIHMIU", "0715414407", "bgichimn@eaconcontracting.co.ke"),
        ("DANIELL KIABI", "0728776794", "dci@dinconkenya.com"),
        ("Laura Musavi", "0748410310/0714059407", "info@jinsing.net"),
        ("Eric Wanjau", "0722883949", "into@tulsi.co.ke"),
        ("Ramila Patel", "0722344844", "admin@nirmaholdings.com"),
        ("Vimal Venkay", "0712652679", "gadapali@gmail.com"),
        ("Jimmy Ji", "0724593434/0786732627", "kenyaoffice@cjic.cc"),
        ("Deng Tiqiang", "0724770581", "csceckenya@chinaconstruction.com"),
        ("Benson Onduso", "0759110330", "crjekenya@gmail.com,sunshinexinxinwu@gmail.com"),
        ("RITA NUNGARI", "0724100019", "RITANUNGARIE@GMALL.COM"),
        ("Manraj MATON", "0722866369", "manraj"),
        ("ZAKAYO EMONG'OCE", "0718881676", "info@cyt.co.ke"),
        ("JAMES ODEGI", "0726916060", "info@lbl.co.ke"),
        ("Martin Ogola", "0734000064", "info@epco.co.ke"),
        ("Thomas Njuguna", "0795711697", "info@2xkenya.com"),
        ("Suraj Vekaria", "0729268206", "info@parklaneconstruction.co.ke"),
        ("MWANIKI HELEN", "0717239007/0720793007", "info@arconworks.com"),
    ];

    let mut file = File::create("contacts.vcf")?;

    for (name, phone, email) in contacts {
        writeln!(file, "BEGIN:VCARD")?;
        writeln!(file, "VERSION:3.0")?;
        writeln!(file, "FN:{}", name)?;

        // Split the phone numbers by '/' and write them as separate TEL entries
        for number in phone.split('/') {
            writeln!(file, "TEL;TYPE=CELL:{}", number.trim())?;
        }

        writeln!(file, "EMAIL:{}", email)?;
        writeln!(file, "END:VCARD")?;
    }

    println!("VCF file created successfully.");
    Ok(())
}
