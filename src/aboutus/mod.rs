use makepad_widgets::Cx;

pub mod about_us;

pub fn live_design(cx: &mut Cx) {
    about_us::live_design(cx);
}
