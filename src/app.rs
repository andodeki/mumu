use crate::{data::state::State, home::editor::fish_doc::*};
use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;
    import crate::app_ui::AppUI;

    App = {{App}} {

        ui: <Window> {
            show_bg: true
            width: Fill,
            height: Fill,
            padding: 0.,
            margin: 0.,
            draw_bg: {
                fn pixel(self) -> vec4 {
                    let Pos = floor(self.pos*self.rect_size *0.10);
                    let PatternMask = mod(Pos.x + mod(Pos.y, 2.0), 2.0);
                    return vec4(0.03,0.03,0.03,1);
                }
            }

            caption_bar = {
                visible: true,
                caption_label = {label ={text: "TiNRS BigFish" }}
            };

            window_menu = {
                main = Main {items: [app, file]}
                app = Sub {name: "TiNRS BigFish", items: [about, line, settings, line, quit]}
                file = Sub {name: "File", items: [newfile]}
                about = Item {name: "About BigFish", enabled: true}
                settings = Item {name: "Settings", enabled: true}
                quit = Item {name: "Quit BigFish", key: KeyQ}
                newfile = Item {name: "New", key: KeyN}
                line = Line,
            }

            window: {inner_size: vec2(800, 600)},
            body = {
                flow: Overlay
                width: Fill,
                height: Fill,

                root = <View> {
                    width: Fill,
                    height: Fill,
                    app_ui = <AppUI>{}
                }
            }
        }
    }
}

app_main!(App);

#[derive(Live, LiveHook)]
pub struct App {
    #[live]
    ui: WidgetRef,
    #[rust]
    counter: usize,
    #[rust]
    state: State,
    // #[rust]
    // document: FishDoc,
}

impl LiveRegister for App {
    fn live_register(cx: &mut Cx) {
        crate::makepad_widgets::live_design(cx);
        // crate::editor::live_design(cx);
        crate::app_ui::live_design(cx);
        crate::app_mobile::live_design(cx);
        crate::app_web::live_design(cx);

        crate::home::live_design(cx);
        crate::aboutus::live_design(cx);
        crate::shared::live_design(cx);
    }
}

impl MatchEvent for App {
    fn handle_startup(&mut self, cx: &mut Cx) {
        let mobile_screen = self.ui.view(id!(mobile));
        let web_screen = self.ui.view(id!(web));
        if self.state.screen_width < 960_f64 {
            log!("SCREEN_WIDTH ===> {}", self.state.screen_width);
            web_screen.set_visible(false);
            mobile_screen.set_visible_and_redraw(cx, true);
            // web_screen.draw_all(cx, &mut Scope::empty());
        } else if self.state.screen_width > 960_f64 {
            mobile_screen.set_visible(false);
            web_screen.set_visible_and_redraw(cx, true);
            // web_screen.draw_all(cx, &mut Scope::empty());
        } else {
            log!("SCREEN_WIDTH ===> {}", self.state.screen_width);
        }

        self.state.document = FishDoc::create_test_doc();
    }

    fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions) {
        // fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
        if self.ui.button(id!(button1)).clicked(&actions) {
            self.counter += 1;
            let label = self.ui.label(id!(label1));
            // label.set_text_and_redraw(cx, &format!("Counter: {}", self.counter));
        }

        if self.ui.button(id!(undobutton)).clicked(&actions) {
            let _ = self.state.document.undo().is_ok();
            self.ui.widget(id!(patchedit)).redraw(cx);
        }

        if self.ui.button(id!(redobutton)).clicked(&actions) {
            let _ = self.state.document.redo().is_ok();
            self.ui.widget(id!(patchedit)).redraw(cx);
        }
        if self.ui.button(id!(addblockbutton)).clicked(&actions) {
            let _ = self.state.document.add_block().is_ok();
            self.ui.widget(id!(patchedit)).redraw(cx);
        }
        for action in actions {
            if let WindowAction::WindowGeomChange(ce) = action.as_widget_action().cast() {
                self.state.screen_width = ce.new_geom.inner_size.x * ce.new_geom.dpi_factor;
                let mobile_screen = self.ui.view(id!(mobile));
                let web_screen = self.ui.view(id!(web));
                if self.state.screen_width < 960_f64 {
                    web_screen.set_visible(false);
                    mobile_screen.set_visible_and_redraw(cx, true);
                } else if self.state.screen_width > 960_f64 {
                    mobile_screen.set_visible(false);
                    web_screen.set_visible_and_redraw(cx, true);
                } else {
                    log!("ELSE_SCREEN_WIDTH ===> {}", self.state.screen_width);
                }
            }
        }
    }
}

impl AppMain for App {
    // fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
    //     self.match_event(cx, event);
    //     self.ui.handle_event(cx, event, &mut Scope::empty());
    // }
    fn handle_event(&mut self, cx: &mut Cx, event: &Event) {
        self.match_event(cx, event);
        self.ui
            .handle_event(cx, event, &mut Scope::with_data(&mut self.state.document));
    }
}
