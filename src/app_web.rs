use makepad_widgets::*;

// use crate::data::state::State;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::home::home_screen::HomeScreen;
    import crate::aboutus::about_us::AboutUs;

    import crate::shared::widgets::NavbarMenuButton;

    ICON_DISCOVER = dep("crate://self/resources/icons/discover.svg")
    ICON_CHAT = dep("crate://self/resources/icons/chat.svg")
    ICON_MY_MODELS = dep("crate://self/resources/icons/my_models.svg")

    AppWebUI = <ScrollXYView>{
        width: Fill,
        height: Fill,
        align: {x: 0.5},
        flow: Down,
        padding: {top: 0, bottom: 0 }
        // debug: A

        navbar_menu = <RoundedView> {
            width: Fill,
            height: Fit,
            flow: Right, spacing: 20.0,
            padding: { top: 0 }
            align: {x: 0.5, y: 0.0},

            show_bg: true,
            draw_bg: {
                color: (SIDEBAR_BG_COLOR),
                instance radius: 0.0,
                border_color: #EAECF0,
                border_width: 1.2,
            }
            home_screen_tab = <NavbarMenuButton> {
                animator: {selected = {default: on}}
                text: "Home",
                draw_icon: {
                    svg_file: (ICON_DISCOVER),
                }
            }
            aboutus_screen_tab = <NavbarMenuButton> {
                text: "About Us",
                draw_icon: {
                    svg_file: (ICON_MY_MODELS),
                }
            }

        }
        application_pages = <View> {
            margin: 0.0,
            padding: 0.0,
            flow: Overlay,
            width: Fill,
            height: Fill,
            // debug: A

            home_screen_frame = <HomeScreen> {align: {x: 0.5, y: 0.5}, visible: true}
            aboutus_screen_frame = <AboutUs> {visible: false}
        }
    }
}
// }

// #[derive(Live, Widget)]
// pub struct AppWebUI {
//     #[deref]
//     ui: Window,
//     #[rust]
//     counter: usize,
//     #[rust]
//     document: FishDoc,
// }

// impl LiveHook for AppWebUI {
//     fn after_new_from_doc(&mut self, _cx: &mut Cx) {
//         self.document = FishDoc::create_test_doc();
//         println!("after_new_from_doc(): starting some kind of a loop");
//     }
// }

// impl Widget for AppWebUI {
//     fn handle_event(&mut self, cx: &mut Cx, event: &Event, scope: &mut Scope) {
//         self.ui
//             .handle_event(cx, event, &mut Scope::with_data(&mut self.document));
//         self.widget_match_event(cx, event, scope);
//     }
//     fn draw_walk(&mut self, cx: &mut Cx2d, scope: &mut Scope, walk: Walk) -> DrawStep {
//         let editor_screen = self.ui.view(id!(patcheditorscreen));
//         while let Some(_next) = self.ui.draw(cx, scope).step() {
//             editor_screen.draw_all(cx, scope);
//         }
//         DrawStep::done()
//     }
// }
// impl WidgetMatchEvent for AppWebUI {
//     fn handle_actions(&mut self, cx: &mut Cx, actions: &Actions, _scope: &mut Scope) {
//         if self.ui.button(id!(button1)).clicked(&actions) {
//             self.counter += 1;
//             let label = self.ui.label(id!(label1));
//             label.set_text_and_redraw(cx, &format!("Counter: {}", self.counter));
//         }

//         if self.ui.button(id!(undobutton)).clicked(&actions) {
//             let _ = self.document.undo().is_ok();
//             self.ui.widget(id!(patchedit)).redraw(cx);
//         }

//         if self.ui.button(id!(redobutton)).clicked(&actions) {
//             let _ = self.document.redo().is_ok();
//             self.ui.widget(id!(patchedit)).redraw(cx);
//         }
//         if self.ui.button(id!(addblockbutton)).clicked(&actions) {
//             let _ = self.document.add_block().is_ok();
//             self.ui.widget(id!(patchedit)).redraw(cx);
//         }
//     }
// }
