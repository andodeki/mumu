pub mod block_connector_button;
pub mod block_delete_button;
pub mod block_header_button;
pub mod fish_block;
pub mod fish_block_editor;
pub mod fish_block_template;
pub mod fish_connection;
pub mod fish_connection_widget;
pub mod fish_doc;
pub mod fish_param_storage;
pub mod fish_patch;
pub mod fish_patch_editor;
pub mod fish_ports;
pub mod fish_preset;
pub mod fish_selector_widget;
pub mod fish_theme;

use makepad_widgets::Cx;

pub fn live_design(cx: &mut Cx) {
    crate::home::editor::fish_patch_editor::live_design(cx);
    crate::home::editor::block_header_button::live_design(cx);
    crate::home::editor::block_delete_button::live_design(cx);
    crate::home::editor::block_connector_button::live_design(cx);
    crate::home::editor::fish_block_editor::live_design(cx);
    crate::home::editor::fish_connection_widget::live_design(cx);
    crate::home::editor::fish_selector_widget::live_design(cx);
    crate::home::editor::fish_theme::live_design(cx);
}
