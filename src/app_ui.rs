use makepad_widgets::*;

live_design! {
    import makepad_widgets::base::*;
    import makepad_widgets::theme_desktop_dark::*;

    import crate::shared::styles::*;
    import crate::app_web::AppWebUI;
    import crate::app_mobile::AppMobileUI;

    // AppUI = {{AppUI}} {
    AppUI = <View> {
        width: Fill,
        height: Fill,
        align: {x: 0.5},
        padding: {top: 0, bottom: 0 }
        // debug: A

        <View> {
            width: Fill,
            height: Fill,
            mobile = <AppMobileUI>{}
            web = <AppWebUI>{}
        }
    }
}
